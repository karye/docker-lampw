# Docker LAMPW
## Building the image

```bash
docker login registry.gitlab.com -u karye -p ...
docker pull registry.gitlab.com/karye/docker-lampw
docker run --restart unless-stopped -d -p 80:80 -p 3000:3000 -p 443:443 -p 10000:10000 -v /var/www:/var/www -v mysql-data:/var/lib/mysql --name lampw registry.gitlab.com/karye/docker-lampw
```

## With new 0.8.0 version
Alternatively, you can also specify the values for the environment variables using the -e flag when starting the container. For example:

```bash
docker run -e AREA=Europe -e CITY=Stockholm -d -p 80:80 -p 3000:3000 -p 443:443 -p 10000:10000 -v /var/www:/var/www -v mysql-data:/var/lib/mysql --name lampw registry.gitlab.com/karye/docker-lampw
```

## Build from source

```bash
git clone https://gitlab.com/karye/docker-lampw.git
cd docker-lampw
docker build -t lampw2 .
docker run --restart unless-stopped -d -p 80:80 -p 3000:3000 -p 443:443 -p 10000:10000 -v /var/www:/var/www -v mysql-data:/var/lib/mysql --name lampw lampw2
```