# ![Docker-LAMPW][logo]
Docker-LAMPW is a LAMP stack ([Apache][apache], [MariaDB][mariadb] and [PHP][php]).

Component | `latest-2004`
---|---
[Apache][apache] |`2.4`
[MariaDB][mariadb] |`15`
[PHP][php] |`8.0`
[phpMyAdmin][phpmyadmin] |`5.2.1`
[Webmin][webmin] |`1.984`
[nodejs][nodejs] |`18`

## Using the image
### Open ports
* Web is accessed on port 80 and 443

### Launching in Linux
```bash
# /var/www i mapped to /home/user/github
# Launch the image with autostart when docker start
docker run --restart unless-stopped -d -p 80:80 -p 443:443 -p 3000:3000 -p 10000:10000 \
-v /var/www:/var/www -v mysql-data:/var/lib/mysql --name lamp registry.gitlab.com/karye/docker-lampw

# You can also set timezone area and city
docker run --restart unless-stopped -d -p 80:80 -p 443:443 -p 3000:3000 -p 10000:10000 \
-v /var/www:/var/www -v mysql-data:/var/lib/mysql -e AREA=Europe -e CITY=Stockholm --name lamp registry.gitlab.com/karye/docker-lampw
```

### Launching in Windows
```powershell
# /var/www i mapped to C:\github
# Launch the image with autostart when docker start
docker run -d --restart unless-stopped -p 80:80 -p 443:443 -p 3000:3000 -p 10000:10000  `
-v "C:\github:/var/www" -v "mysql-data:/var/lib/mysql" --name lamp registry.gitlab.com/karye/docker-lampw
```

### Test installation
Launch `http://localhost/lampw` to test webbserver is up and list setup info.

## Useful Docker commands
```bash
# View logg
docker logs lamp

# Attach to console
docker exec -it lamp bash

# View running containers
docker ps -a

# Updating to latest image
# Pull latest image from https://registry.gitlab.com/karye/docker-lampw
docker stop karye/lampw
docker rm karye/lampw
docker pull karye/lampw
```

## User & password
Password for user admin is generated randomly see file in root (/)
A file is created in / named as **admin:pass**

## Project layout
### The webroot
The url `http://localhost` is mapped to '/var/www/' in the docker container.
```bash
/ (project root)
/var/www/ (your folders and files live here)
```

### webmin	
Docker-LAMPW comes pre-installed with Webmin available at `https://localhost:10000`.\	
Login in with user **root** and password **pass**.\	
Local timezone is 'Europe/Stockholm'. You may change timezone in Webmin.

### The databases
Database data is located on a separate volume for datapersistens.
The databases will therefore not be lost when reinstalling or upgrading LAMPW. 

## Administration
### PHPMyAdmin
Docker-LAMPW comes pre-installed with phpMyAdmin available at `http://localhost/phpmyadmin`.\
Login in with user **admin** and password **pass**.

## License
Docker-LAMPW is licensed under the [Apache 2.0 License][info-license].

[logo]: https://cdn.rawgit.com/mattrayner/docker-lamp/831976c022782e592b7e2758464b2a9efe3da042/docs/logo.svg

[apache]: http://www.apache.org/
[mariadb]: https://mariadb.org/
[php]: http://php.net/
[phpmyadmin]: https://www.phpmyadmin.net/
[Webmin]: http://www.webmin.com/
[nodejs]: https://nodejs.org/en/

[end-of-life]: http://php.net/supported-versions.php
[info-docker-hub]: https://hub.docker.com/r/mattrayner/lamp
[info-license]: LICENSE
