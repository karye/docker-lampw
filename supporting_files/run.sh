#!/bin/bash

# Set locale
#export LANG=sv_SE.UTF-8
#export LC_MESSAGES=en_US.UTF-8

# Setting up cron jobs
echo "0 * * * * root rsync -a /etc/apache2 /home/" > /etc/crontab
echo "0 * * * * root rsync -a /etc/letsencrypt /home/" >> /etc/crontab

# Set timezone
ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone

# Set PHP timezone and file sizes
if [ -e /etc/php/${PHP_VERSION}/apache2/php.ini ]
then
    echo "=> Setting PHP filesizes and timezone"
    sed -ri -e "s|^upload_max_filesize.*|upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}|" \
        -e "s|^post_max_size.*|post_max_size = ${PHP_POST_MAX_SIZE}|" \
        -e "s|^;date.timezone =*|date.timezone = \"${TIMEZONE}\"|" \
        /etc/php/${PHP_VERSION}/apache2/php.ini
fi

#sed -i "s|export APACHE_RUN_GROUP=www-data|export APACHE_RUN_GROUP=www-data|" /etc/apache2/envvars

# Set MySQL security
sed -i -e "s|cfg\['blowfish_secret'\] = ''|cfg['blowfish_secret'] = '`date | md5sum`'|" /var/phpmyadmin/config.inc.php

# Setting permissions
echo "=> Setting directories permissions and owners"
if [ -n "$VAGRANT_OSX_MODE" ]
then
    usermod -u $DOCKER_USER_ID www-data
    groupmod -g $(($DOCKER_USER_GID + 10000)) $(getent group $DOCKER_USER_GID | cut -d: -f1)
    groupmod -g ${DOCKER_USER_GID} www-data
else
    # Tweaks to give Apache/PHP write permissions
    chown -R www-data:www-data /var/phpmyadmin
fi

# Set permissions
chown -R www-data:www-data /var/lib/mysql /var/run/mysqld /var/www
chmod -R 770 /var/lib/mysql /var/run/mysqld /var/www

# Set MySQL bind address
sed -i "s|bind-address.*|bind-address = 0.0.0.0|" /etc/mysql/my.cnf
sed -i "s|.*bind-address.*|bind-address = 0.0.0.0|" /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i "s|user.*|user = www-data|" /etc/mysql/my.cnf

# Create log file
/usr/bin/touch /var/webmin/miniserv.log

# Start MySQL
/usr/bin/mysqld_safe > /dev/null 2>&1 &
RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MySQL service startup"
    sleep 5
    #mysql -uroot -e "status" > /dev/null 2>&1
    ps aux | grep mysql
    RET=$?
done

# Generate random password
PASS=$(/usr/bin/pwgen 6 1)
rm /root/admin:*
touch "/root/$ADMIN:$PASS"
htpasswd -b -c /etc/apache2/.htpasswd ${ADMIN} ${PASS}
echo root:${PASS} | chpasswd

echo "=> Creating MySQL admin user with $PASS password"
mysql -uroot -e "GRANT ALL ON *.* TO '${ADMIN}'@'localhost' IDENTIFIED BY '${PASS}' WITH GRANT OPTION"
mysql -uroot -e "FLUSH PRIVILEGES"
echo "========================================================================"
echo "You can now connect to this MySQL Server with $PASS"
echo ""
echo "    mysql -uadmin -p$PASS -h<host> -P<port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "MySQL user 'root' has no password but only allows local connections"
echo ""
mysqladmin -uroot shutdown
echo "=> Shutting down MySQL"

# Install the OWASP Core Rule Set
mv /tmp/coreruleset-3.3.4 /etc/apache2/modsecurity-crs/

# Fix /etc/modsecurity/modsecurity.conf
mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
sed -i "s|SecRuleEngine DetectionOnly|SecRuleEngine On|" /etc/modsecurity/modsecurity.conf
sed -i "s|ABDEFHIJZ|ABCEFHJKZ|" /etc/modsecurity/modsecurity.conf

# Start supervisord and services
exec supervisord -n

# Remove local files
rm -rf /tmp/*
#rm -- "$0"