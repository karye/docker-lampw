FROM buildpack-deps:focal-curl

ARG PHP_VERSION=8.0
ARG PHPMYADMIN_VERSION=5.2.1
ARG AREA=Europe
ARG CITY=Stockholm

# Set environment variables
ENV ADMIN "admin"
ENV PASS "pass"
ENV LANG C.UTF-8
ENV REFRESHED_AT 2022-02-20
ENV DOCKER_USER_ID 501 
ENV DOCKER_USER_GID 20
ENV BOOT2DOCKER_ID 1000
ENV BOOT2DOCKER_GID 50
ENV TIMEZONE ${AREA}/${CITY}
ENV DEBIAN_FRONTEND noninteractive

# Environment variables to configure php
ENV PHP_UPLOAD_MAX_FILESIZE=10M
ENV PHP_POST_MAX_SIZE=10M

# Tweaks to give Apache/PHP write permissions
RUN usermod -u ${BOOT2DOCKER_ID} www-data; \
    usermod -G www-data www-data; \
    useradd -r mysql; \
    usermod -G www-data mysql; \
    groupmod -g $(($BOOT2DOCKER_GID + 10000)) $(getent group ${BOOT2DOCKER_GID} | cut -d: -f1); \
    groupmod -g ${BOOT2DOCKER_GID} www-data

# Upgrade to latest packages
# Set environment variables
# RUN locale-gen en_US.UTF-8
RUN echo debconf debconf/frontend select Noninteractive | debconf-set-selections; \
    apt update && apt -y upgrade && \
    apt -y install --no-install-recommends language-pack-sv lsb-release software-properties-common apt-transport-https apt-utils

# Install apache2 & php & mariadb
RUN add-apt-repository -y ppa:ondrej/php && \
    apt install -yq --no-install-recommends libapache2-mod-security2 php${PHP_VERSION} php${PHP_VERSION}-cli \
    php${PHP_VERSION}-common php${PHP_VERSION}-curl \
    php${PHP_VERSION}-mysql php${PHP_VERSION}-readline \
    php${PHP_VERSION}-xml php${PHP_VERSION}-xsl php${PHP_VERSION}-gd php${PHP_VERSION}-intl \
    php${PHP_VERSION}-bz2 php${PHP_VERSION}-bcmath php${PHP_VERSION}-gd \
    php${PHP_VERSION}-mbstring php${PHP_VERSION}-xmlrpc php${PHP_VERSION}-zip libapache2-mod-php${PHP_VERSION} \
    nano supervisor less wget git apache2 mariadb-server pwgen unzip curl cron \
    certbot python3-certbot-apache gnupg2 python3-pip && \
    apt -y autoremove

# Needed for phpMyAdmin and wordpress
RUN phpenmod mbstring; \
    a2enmod rewrite; \
    a2enmod security2; \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Add phpmyadmin
RUN wget -O /tmp/phpmyadmin.tar.gz https://files.phpmyadmin.net/phpMyAdmin/${PHPMYADMIN_VERSION}/phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages.tar.gz; \
    tar xfvz /tmp/phpmyadmin.tar.gz -C /var; \
    mv /var/phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages /var/phpmyadmin; \
    mv /var/phpmyadmin/config.sample.inc.php /var/phpmyadmin/config.inc.php

# Add webmin
RUN echo "Acquire::GzipIndexes \"false\"; Acquire::CompressionTypes::Order:: \"gz\";" > /etc/apt/apt.conf.d/docker-gzip-indexes; \
    wget -O /tmp/jcameron-key.asc http://www.webmin.com/jcameron-key.asc && apt-key add /tmp/jcameron-key.asc; \
    echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list; \
    echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" >> /etc/apt/sources.list; \
    apt update && apt install -y --no-install-recommends webmin && apt clean && rm -rf /var/lib/apt/lists/*

# Add nodejs 18
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -; \
    apt install -y nodejs && npm i pm2@latest -g

# Install mkdocs & material
RUN pip3 install mkdocs mkdocs-material pymdown-extensions mkdocs-awesome-pages-plugin mdx_truly_sane_lists

# Install the OWASP Core Rule Set
RUN wget -O /tmp/v3.3.4.tar.gz https://github.com/coreruleset/coreruleset/archive/refs/tags/v3.3.4.tar.gz; \
    tar xvzf /tmp/v3.3.4.tar.gz -C /tmp

# Add volumes for the webroot and persistent mysql data
# Add volumes for user data and apache config
VOLUME ["/home"]
VOLUME ["/var/www"]
VOLUME ["/var/lib/mysql"]

# Add image configuration and scripts
COPY supporting_files/supervisord-apache2.conf /etc/supervisor/conf.d/supervisord-apache2.conf
COPY supporting_files/supervisord-mysqld.conf /etc/supervisor/conf.d/supervisord-mysqld.conf
COPY supporting_files/supervisord-webmin.conf /etc/supervisor/conf.d/supervisord-webmin.conf
COPY supporting_files/supervisord-cron.conf /etc/supervisor/conf.d/supervisord-cron.conf
COPY supporting_files/run.sh /root/run.sh
RUN  chmod 755 /root/run.sh; mkdir /var/lampw

# Config to enable .htaccess
# Secure web root
COPY supporting_files/000-default.conf /etc/apache2/sites-available/
COPY supporting_files/.htaccess /var/www/
COPY supporting_files/.bashrc /root/
COPY supporting_files/inputrc /etc/
COPY supporting_files/security2.conf /etc/apache2/mods-enabled/

# Add homepage
COPY app/index.php /var/lampw
COPY app/style.css /var/lampw
COPY app/lamp.svg /var/lampw

# Open ports in firewall
EXPOSE 80 443 3000 10000

CMD ["/root/run.sh"]
