# TODO
## SSL
How To Secure Apache with Let's Encrypt on Ubuntu 18.04  
https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-18-04

```bash
sudo apt install certbot python3-certbot-apache
sudo certbot --apache
```

## Add port 443
```bash
docker run --restart unless-stopped -d -p 80:80 -p 443:443 -v /var/www:/var/www -v mysql-data:/var/lib/mysql --name lamp registry.gitlab.com/karye/docker-lampw
```

## Activate basic Auth
```bash
nano /etc/apache2/apache2.conf
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

## Backup settings
* Try rsnapshot https://ubuntu.com/server/docs/tools-rsnapshot

## Security
* Options -Indexes -FollowSymLinks -MultiViews
* https://www.linuxbabe.com/security/modsecurity-apache-debian-ubuntu

```bash
a2enmod headers
apt install libapache2-mod-security2
/etc/init.d/apache2 restart
```

## Here are a few suggestions to make this Dockerfile better:

* Use a smaller base image, such as FROM ubuntu:20.04 to FROM buildpack-deps:focal. This can help reduce the size of your image.

* Group related commands together, such as installing package dependencies and installing packages. This can make the Dockerfile more readable and easier to maintain.

* Use the RUN command with the --no-install-recommends flag to avoid installing unnecessary packages.

* Use the RUN command with the apt-get clean command to remove unnecessary files after installation, which can help reduce the size of your image.

* Use the ARG command to define build-time variables, rather than using ENV. This can help improve the security of your image, as build-time variables are not persisted in the final image.

* Use the COPY command to copy files from the context to the filesystem of the container. This can help improve the performance of the build, as it avoids the need to download files over the network.

* Use the EXPOSE command to specify the ports that should be exposed by the container. This can help users of the image understand which ports are available for use.

* Use the USER command to specify the user that should be used to run the command in the container. This can help improve the security of the container by running processes as a non-root user.

* Use the HEALTHCHECK command to specify how the container can be checked for health. This can help improve the reliability of your container by allowing it to be automatically restarted if it becomes unhealthy.

* Use the ENTRYPOINT command to specify the default command that should be run when the container is started. This can help users of the image understand how to use it.

## Set Locale att runtime

Then, when you build the image, you can specify the values for the AREA and CITY variables using the --build-arg flag. For example:

```bash
docker build --build-arg AREA=Asia --build-arg CITY=Tokyo .
```

This would set the TIMEZONE environment variable to Asia/Tokyo. If you do not specify values for the AREA and CITY variables, the default values defined in the ARG command will be used.

Alternatively, you can also specify the values for the environment variables using the -e flag when starting the container. For example:

```bash
* docker run -e AREA=Asia -e CITY=Tokyo my_image
```

This would set the TIMEZONE environment variable to Asia/Tokyo when the container is started.